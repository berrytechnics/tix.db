var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Ticket = new Schema({
    number:Number,
    customer:String,
    signature:{
      data:String,
      date:Date
    },
    device:{
      make:String,
      model:String,
      color:String,
      carrier:String,
      serial:String,
      simIncluded:Boolean
    },
    issue:String,
    notes:Array,
    lineItems:Array,
    openedBy:String
});

// Ticket.plugin(passportLocalMongoose);
module.exports = mongoose.model('Ticket', Ticket);
