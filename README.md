# Tix.db
#### https://tixdb.herokuapp.com
#### https://www.npmjs.com/package/tix.db-cellphone_repair
### About
> Tix.db is an open-source ticketing software for
> repair shops built on the MEN software stack.
> If you know web design and javascript, you can likely setup,
>  modify, and use this software.


### Setup & Tests
* you must have node >= 9.1 installed
* you must be running a local MongoDB server >= 3.4
* clone repo and `npm install`, or `yarn` (I recommend Yarn)
* `npm test` or `yarn test`

> For all tests requiring user credentials, your database must have
> a user with username : admin , password : admin

> The 'new ticket' test requires a customer to be in your database with `_id:objectId('5b1344a7a0c43f2c52f6cf5b')`
> Make sure you either have a matching customer in your DB, or modify (/test/test.js:131:15)
> to match an existing customer, or your Ticket tests will not pass .

### Current Working Modules (CRUD)
* Users
* Customers
* Tickets

### Known Bugs

### ToDo Modules
* Invoices
* Admin
* Reports
* Tech/CSR/Admin Dashboards

#### Used packages
* [Async](https://github.com/caolan/async "Async Github page")
* [Express](https://github.com/expressjs/express "Express Github page")
* [Mongoose](https://github.com/Automattic/mongoose "Mongoose Github page")
* [Passport](https://github.com/jaredhanson/passport "Passport Github page")
* [Signature Pad](https://github.com/szimek/signature_pad "Signature Pad's Github page")
* [Moment](https://momentjs.com "MomentJS.com")
