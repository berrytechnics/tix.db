var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var passportLocalMongoose = require('passport-local-mongoose');

var Customer = new Schema({
    name:String,
    address:{
      street:String,
      city:String,
      state:String,
      zip: Number
    },
    phone:Array,
    email:Array,
    tickets:Array
});

// Customer.plugin(passportLocalMongoose);
module.exports = mongoose.model('Customer', Customer);
