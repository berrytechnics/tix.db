var express = require('express');
var mongoose = require('mongoose');
var User = require('../models/users');
var Customer = require('../models/customers');
var passport = require('passport');
var router = express.Router();
var flash = require('connect-flash');
var db = mongoose.connection;


router.get('/find/:id',(req,res,next)=>{
  var id = req.params.id
  if(parseInt(id) > -1){
    Customer.find(
      { "phone": { "$regex": id, "$options": "i" } },
      function(err,customer) {
        !err ? res.send(customer) : res.send(err)
      })
  }
  else if(id){
    Customer.find(
      { "name": { "$regex": id, "$options": "i" } },
      function(err,customer) {
        !err ? res.send(customer) : res.send(err)
      })
  }
});

router.get('/', function(req,res,next){
  Customer.find({},(err,customers)=>{
    res.render('customers/index',{customers:customers,user:req.user})
  })
})

router.get('/view/:id', function(req, res, next) {
  var id = req.params.id;
  Customer.findOne({_id:id},(err,customer)=>{
    if(!err) res.render('customers/view',{customer:customer,user:Customer})
    else{
      req.flash('error',err)
      res.redirect('/customers')
    }
  })
})

router.get('/new',(req,res,next)=>{
  res.render('customers/new')
})

router.get('/edit/:id',(req,res,next)=>{
  Customer.findOne({_id:req.params.id},(err,customer)=>{
    if(!err) res.render('customers/edit',{customer:customer,user:req.user})
    else{
      req.flash('error',err)
      res.redirect(`/customers/view/${req.params.id}`)
    }
  })
})

router.post('/new',(req,res,next)=>{
  var customer = {
    "name":req.body.name,
    "phone":[],
    "email":[],
    "address":{
      "street":req.body.street,
      "city":req.body.city,
      "state":req.body.state,
      "zip":req.body.zip
    }
  }
  var filteredPhones = Object.keys(req.body).filter((entry) => /phone/.test(entry));
  var filteredEmails = Object.keys(req.body).filter((entry) => /email/.test(entry));
  filteredPhones.forEach(number=>customer.phone.push(req.body[number]))
  filteredEmails.forEach(email=>customer.email.push(req.body[email]))
  Customer.create(customer,(err,result)=>{
    if(!err){
      req.flash('success','Customer Added!')
      res.redirect(`/customers/view/${result._id}`)
    }
    else{
      req.flash('error',err)
      res.render('customers/new',{customer:customer,user:req.user})
    }
  })
})

router.post('/edit',(req,res,next)=>{
  Customer.update({_id:req.body.id},req.body.customer,err=>{
    if(!err) res.send('Customer Updated')
    else res.send({err:JSON.stringify(err)})
  })
})

router.delete('/delete/:id',(req,res,next)=>{
  Customer.remove({_id:req.params.id},(err)=>{
    if(!err){
      req.flash('success','Customer Deleted!')
      res.redirect('/customers')
    }
    else{
      req.flash('error',JSON.stringify(err))
      res.redirect(`/customer/view/${req.params.id}`)
    }
  })
})

module.exports = router;
