module.exports.isAdmin = (req,res,next)=>{
  if(req.user && req.user.usergroup.includes('admin')) next()
  else{
    req.flash('error','Admin access only!')
    res.redirect('/')
  }
}
module.exports.isManager = (req,res,next)=>{
  if(req.user && req.user.usergroup.includes('manager')) next()
  else{
    req.flash('error','Manager access only!')
    res.redirect('/')
  }
}
module.exports.isTech = (req,res,next)=>{
  if(req.user && req.user.usergroup.includes('technician')) next()
  else{
    req.flash('error','Technician access only!')
    res.redirect('/')
  }
}
module.exports.isCsr = (req,res,next)=>{
  if(req.user && req.user.usergroup.includes('csr')) next()
  else{
    req.flash('error','CSR access only!')
    res.redirect('/')
  }
}
module.exports.isLoggedIn = (req,res,next)=>{
  if(req.user) next()
  else{
    req.flash('error','You must login first!')
    res.redirect('/users/login')
  }
}
