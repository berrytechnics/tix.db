var express = require('express');
var mongoose = require('mongoose');
var User = require('../models/users');
var passport = require('passport');
var router = express.Router();
var flash = require('connect-flash');
var db = mongoose.connection;

router.get('/login', function(req, res, next) {
    res.render('login',{user:req.user})
});
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});
router.get('/register',(req,res,next)=>{
  res.render('register',{user:req.user})
})
router.post('/register', function(req, res) {
    if(req.body.password === req.body.confirm){
      User.register(new User({
         name : req.body.name ,
         username : req.body.username,
         email: req.body.email,
         phone: req.body.phone,
         usergroup: 'newb',
       }), req.body.password, function(err, user) {
          if (err) {
              req.flash('error',err)
              return res.render('register', { user : user });
          }
          passport.authenticate('local')(req, res, function () {
              req.flash('success','You have been registered!')
              res.redirect('/');
          });
      });
    }
    else res.send('bad confirm')
});
router.post('/login', passport.authenticate('local', {
   successRedirect: '/',
   failureRedirect: '/users/login',
   successFlash: 'You were logged in!',
   failureFlash: 'Invalid Username/Password'
 }), function(req, res) {
    res.redirect('/users/login')
});

module.exports = router;
