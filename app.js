var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require("express-session");
var passport = require('passport');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var flash = require('express-flash');
var LocalStrategy = require('passport-local').Strategy;
var index = require('./routes/index');
var users = require('./routes/users');
var customers = require('./routes/customers');
var tickets = require('./routes/tickets');
var inventory = require('./routes/inventory');

var app = express();

var env = function(){
    switch(process.env.NODE_ENV){
        case 'development':
            return {
              db:'mongodb://localhost/tixdb'
            };
        case 'production':
            return {
              db:'mongodb://public:password1@ds125016.mlab.com:25016/tixdb'
            };
        default:
            return {
              db:'mongodb://public:password1@ds125016.mlab.com:25016/tixdb'
            };
    }}()

mongoose.connect(env.db);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('MongoDB connected...')
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

var User = require('./models/users');
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  secret: "Apple sucks dick!",
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  resave:true,
  saveUninitialized:false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', index);
app.use('/users', users);
app.use('/customers', customers);
app.use('/tickets', tickets);
app.use('/inventory', inventory);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error',{user:req.user});
});

module.exports = app;
