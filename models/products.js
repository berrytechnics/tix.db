var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = new Schema({
    name:String,
    description:String,
    sku:String,
    image:String,
    cost:Number,
    retail:Number,
    quantity:Number,

});

module.exports = mongoose.model('Product', Product);
