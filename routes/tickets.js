var express  = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var Async    = require('async');
var router   = express.Router();
var flash    = require('connect-flash');
var db       = mongoose.connection;
var User     = require('../models/users');
var Customer = require('../models/customers');
var Ticket   = require('../models/tickets');
var Auth     = require('../models/auth');

router.get('/find/:id',(req,res,next)=>{
  Ticket.findOne({_id:req.params.id},(err,ticket)=>{
    !err ? res.send(ticket) : res.send(JSON.stringify(err))
  })
})

router.get('/',(req,res,next)=>{
  var send = []
  Ticket.find({},(err,tickets)=>{
    Async.eachSeries(tickets, function(ticket,done){
      Customer.findOne({_id:ticket.customer},(err,customer)=>{
        ticket.client = {
          name:customer.name,
          id:customer._id
        }
        send.push(ticket)
        done()
      })}, function(err){
      if(err) {
        req.flash('error',JSON.stringify(err))
        res.redirect('/')
      }
      console.log(send)
      res.render('tickets/index',{tickets:send,user:req.user})
    });
  })
})

router.get('/view/:id', function(req, res, next) {
  var id = req.params.id;
  Ticket.findOne({_id:id},(err,ticket)=>{
    var ticket = ticket
    if(err){
      req.flash('error',JSON.stringify(err))
    }
    else{
      Customer.findOne({_id:ticket.customer},(err,customer)=>{
        ticket.client = {
          name:customer.name,
          phone:customer.phone,
          email:customer.email,
          address:customer.address
        };
        if(!err) res.render('tickets/view',{ticket:ticket,user:req.user})
        else{
          req.flash('error',err)
          res.redirect('/tickets')
        }
      })
    }
  })
})

router.get('/new',(req,res,next)=>{
  res.render('tickets/new',{user:req.user})
})

router.get('/edit/:id',(req,res,next)=>{
  Ticket.findOne({_id:req.params.id},(err,ticket)=>{
    if(!err) res.render('tickets/edit',{ticket:ticket,user:req.user})
    else{
      req.flash('error',err)
      res.redirect('/tickets')
    }
  })
})

router.post('/new',(req,res,next)=>{
  var ticket = {
    number:null,
    customer:req.body.id,
    device:{
      make:req.body.make,
      model:req.body.model,
      color:req.body.color,
      serial:req.body.serial,
      simIncluded:req.body.sim
    },
    issue:req.body.issue,
    notes:[{
      created:Date.now(),
      body:req.body.notes,
      // createdBy:req.user.username
    }],
    lineItems:[],
    // openedBy:req.user.username
  }
  Ticket.create(ticket,(err,result)=>{
    if(!err) {
      req.flash('success','Ticket created!')
      res.redirect(`/tickets/view/${result._id}`)
    }
    else{
      req.flash('error',JSON.stringify(err))
      res.render('tickets/new',{ticket:ticket,user:req.user})
    }
  })
})

router.post('/edit',(req,res,next)=>{
  Ticket.update({_id:req.body.id},{$set:req.body.ticket},err=>{
    if(!err) res.send('Ticket Updated!')
    else res.send({err:err})
  })
})

router.post('/notes/add/:id',(req,res,next)=>{
  if(req.body.notes !== ''){
    var note = {
      number:null,
      created:Date.now(),
      body:req.body.notes,
      createdBy:req.user.username
    }
    Ticket.update({_id: req.params.id },{ $push: {notes:note} },err=>{
      if(!err){
        req.flash('success','Ticket note saved!')
        res.redirect(`/tickets/view/${req.params.id}`)
      }
      else {
        req.flash('error',JSON.stringify(err))
        res.redirect(`/tickets/view/${req.params.id}`)
      }
    })
  }
  else{
    req.flash('error','Submitted note was empty!')
    res.redirect(`/tickets/view/${req.params.id}`)
  }
})
router.delete('notes/delete',(req,res,next)=>{
  res.send('note deleted')
})
router.post('/lineItems/add',(req,res,next)=>{
  res.send('item added')
})
router.delete('/lineItems/delete',(req,res,next)=>{
  res.send('item removed')
})
router.delete('/delete/:id',(req,res,next)=>{
  Ticket.remove({_id:req.params.id},err=>{
    if(!err){
      req.flash('success','Ticket was deleted')
      res.redirect('/tickets')
    }
    else{
      req.flash('error',JSON.stringify(err))
      res.redirect(`/tickets/view/${req.params.id}`)
    }
  })
})

router.get('/getSignature/:id',(req,res,next)=>{
  Ticket.findOne({_id:req.params.id},(err,ticket)=>{
    if(!err) res.send(ticket.signature)
    else res.send({err:err})
  })
})
router.post('/addSignature',(req,res,next)=>{
  Ticket.update({_id:req.body.ticket},{$set:{signature:{data:req.body.signature,date:Date.now()}}},err=>{
    if(!err) res.send('Signature Saved!')
    else res.send('Unable to save signature, please sign paper copy.')
  })
})

module.exports = router;
