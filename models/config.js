var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = new Schema({
    name:String,
    email:String,
    address:{
      street:String,
      city:String,
      state:String,
      zip:Number
    },
    phone:String,
    taxRate:Number,

});

module.exports = mongoose.model('Config', Config);
