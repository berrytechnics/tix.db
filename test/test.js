var cheerio = require('cheerio')
var chai = require('chai');
var chaiCheerio = require('chai-cheerio')
var chaiHttp = require('chai-http');
var server = require('../app');
var chalk = require('chalk');
var should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);
chai.use(chaiCheerio);

describe('User',function(){
  describe('login',()=>{
    it('should allow user to login w/creds. /users/login POST', function(done) {
      chai.request(server).post('/users/login')
        .send({'username':'admin','password':'admin'})
        .end(function(err, res){
          // var $ = cheerio.load(res.text)
          expect(err).to.be.null;
          res.should.have.status(200);
          res.should.be.html;
          res.redirects.forEach(redirect=>{
            expect(redirect).to.not.include('login')
          })
          done();
        })
    })
    it('should prevent login with bad creds. /users/login POST', function(done) {
      chai.request(server).post('/users/login')
        .send({'username':'bad','password':'creds'})
        .end(function(err, res){
          expect(err).to.be.null;
          res.should.have.status(200);
          res.should.be.html;
            expect(res.redirects[0]).to.include('login')
          done();
        })
    })
  })
  // next describe here
})

describe('Customers',()=>{
  var id;
  it('should create new customer with POST data. /customers/new',done=>{
      chai.request(server).post('/customers/new')
      .auth('admin','admin')
        .send({'name':'John Doe',
              'phone':[9184513614],
              'email':['none@mail.com'],
              'address':{
                'street':'123 E Main St.',
                'city':'Tulsa',
                'state':'OK',
                'zip':74136
              }})
        .end(function(err, res){
          expect(err).to.be.null;
          res.should.have.status(200);
          res.should.be.html;
            res.redirects.forEach(redirect=>{
              expect(redirect).to.not.include('new')
            })
            id = res.redirects[0].substr(res.redirects[0].length - 24)
          done();
        })
  })
  it('should return customer with _id as JSON. /customers/find/<_id> GET',done=>{
    chai.request(server).get(`/customers/find/${id}`)
      .end((err,res)=>{
        expect(err).to.be.null;
        res.should.have.status(200);
        res.should.be.json;
        done()
      })
  })
  it('should find a customer with _id. /customers/view/<_id> GET',done=>{
    chai.request(server).get(`/customers/view/${id}`)
      .end((err,res)=>{
        expect(err).to.be.null;
        res.should.have.status(200);
        res.should.be.html;
        done()
      })
  })
  it(`should modify customer with _id. /customers/edit POST`,done=>{
    var customer = {
      name:'Mocha Test'
      }
    chai.request(server)
      .post('/customers/edit')
      .send(JSON.stringify({id:id,customer}))
      .end((err,res)=>{
        expect(res.err).to.not.exist;
        done()
      })
  })
  it(`should delete customer with _id. /customers/delete/<_id> DELETE`,done=>{
    chai.request(server)
      .delete(`/customers/delete/${id}`)
      .end((err,res)=>{
        expect(err).to.be.null
        res.should.have.status(200);
        res.should.be.html;
          res.redirects.forEach(redirect=>{
            expect(redirect).to.not.include(id)
          })
        done()
      })
  })
})

describe('Tickets',()=>{
  var id;
  it('should create a new ticket with sent data',done=>{
    chai.request(server).post('/tickets/new')
      .auth('admin','admin')
      .send({
        'number':'null',
        'id':'5b1344a7a0c43f2c52f6cf5b',
        'make':'Apple',
        'model':'iPhone 6s+',
        'color':'Rose Gold',
        'serial':'23014123456789123',
        'simIncluded':true,
        'issue':'Mocha testing with Chai'
      })
      .then(res=>{
          // expect(err).to.be.null
          res.should.have.status(200);
          res.should.be.html;
            res.redirects.forEach(redirect=>{
              expect(redirect).to.not.include('new')
            })
            id = res.redirects[0].substr(res.redirects[0].length - 24)
            done()
      })
      .catch(err=>{
        if(err) {
          console.log(chalk.bgYellow(`
            This test requires a customer in your DB
            with the _id 5b1344a7a0c43f2c52f6cf5b.
            Make sure you have a customer with this _id,
            or modify this test (/test/test.js:131:15)
            to match an existing customer!
            `))
            done(err)
        }
      })
  })
  it('should find ticket with _id. as JSON /tickets/find/<_id> GET',done=>{
    chai.request(server).get(`/tickets/find/${id}`)
      .end((err,res)=>{
        expect(err).to.be.null
        res.should.have.status(200);
        res.should.be.json;
        expect(res).to.not.redirect
        done()
      })
  })
  it('should find ticket with _id. as HTML /tickets/view/<_id> GET',done=>{
    chai.request(server).get(`/tickets/view/${id}`)
      .end((err,res)=>{
        expect(err).to.be.null
        res.should.have.status(200);
        res.should.be.html;
        expect(res).to.not.redirect
        done()
      })
  })
  it('should modify ticket with _id. /tickets/edit/<_id> POST',done=>{
    var ticket = {device:{make:'Samsung'}}
    chai.request(server).post('/tickets/edit')
      .send(JSON.stringify({id:id,ticket}))
      .end((err,res)=>{
        expect(res.err).to.not.exist
        done()
      })
  })
  it('should delete ticket with _id. /tickets/delete/<_id> DELETE',done=>{
    chai.request(server).delete(`/tickets/delete/${id}`)
      .end((err,res)=>{
        expect(err).to.be.null
        res.should.have.status(200);
        res.should.be.html;
        res.redirects.forEach(redirect=>{
          expect(redirect).to.not.include(id)
        })
        done()
      })
  })
})
